package person.pluto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import person.pluto.natcross.entity.ListenPort;
import person.pluto.natcross.server.NatcrossServer;
import person.pluto.natcross.service.IListenPortService;
import person.pluto.natcross2.serverside.client.ClientServiceThread;

import javax.annotation.Resource;
import java.util.List;

@SpringBootApplication
public class NatcrossBootApplication implements ApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(NatcrossBootApplication.class, args);
    }

    @Resource
    private ClientServiceThread clientServiceThread;

    @Resource
    private IListenPortService listenPortService;

    @Resource
    private NatcrossServer natcrossServer;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        clientServiceThread.start();

        QueryWrapper<ListenPort> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(ListenPort::getOnStart, true);
        List<ListenPort> list = listenPortService.list(queryWrapper);

        for (ListenPort listenPort : list) {
            natcrossServer.createNewListen(listenPort);
        }

    }

}
